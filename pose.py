# IMPORTING LIBRARIES
import cv2
import mediapipe as mp
import os
import numpy as np
import matplotlib.pyplot as plt
import sys

# MEDIAPIPE POSE DETECTION UTILITIES
mpPose = mp.solutions.pose
pose = mpPose.Pose()
mpDraw = mp.solutions.drawing_utils

"""

This function will take an input image/frame as a paramter
and first processes the image using pose.process, here pose is the
Pose() module of mediapipe solutions.pose library.
This processing in mediapipe processes the image and using the results,
we extract the landmarks on the given image/frame. For this purpose, we
used results.pose_landmarks.landmark which stores the landmarks in itself.

32 landmarks will be identified in an human body, those will be stored in a
dictionary and the dictionary will be returned along with the results of the
processing of image/frame.

"""
def detect_landmarks(image_):

    dict_ = {}

    # initializing for 32 landmarks
    for i in range(33):
        dict_[i] = (0,0)

    results = pose.process(image_)
    if results.pose_landmarks:

        for landmark_number, value in enumerate(results.pose_landmarks.landmark):
            h, w, _ = image_.shape
            x_coord, y_coord = int(value.x*w), int(value.y*h)

            # updating dictionary with the co-ordinates detected for the
            # landmark with number landmark_number
            dict_[landmark_number] = (x_coord, y_coord)

    return dict_, results

"""

This function calculates the angle betweem 2 2-D vectors in degrees.
It takes input parameters list t1 and t2 representing the vectors.
First it takes the inner product of the 2 vectors and stores in inner_product
and then it calculates the norms individually for t1 and t2 and store their
product in norm_product. cos_theta is calculated as inner_product/norm_product

Then the angle is calculated first in radians and then converted to degree.

"""
def angle_between(t1, t2):
    inner_product = np.inner(t1, t2)
    norm_product = np.linalg.norm(t1) * np.linalg.norm(t2)

    cos_theta = inner_product / norm_product

    angle_radian = np.arccos(np.clip(cos_theta, -1.0, 1.0))
    angle_deg = np.rad2deg(angle_radian)
    return angle_deg


# function to calculate difference between 2 tuples a and b
def tupple_diff(a,b):
    return [a[0] - b[0], a[1] - b[1]]


"""

This function computes the angles required for the comparison metrics.
It includes 1. angle between the body and the left/right hand, 2. angle formed
by elbows left and right.

The dictionary representing the landmarks are passed as a paramter to the
function. Using the landmarks, different vectors are calculated and the angle
between them is then further calculated and returned.

"""
def computeAngleLR(testDic):

    ##############################################################
    # First computing angle between body and hand

    right_shoulder = testDic[12]
    right_elbow = testDic[14]

    # right hand vector
    right_hand_vector = tupple_diff(right_elbow, right_shoulder)
    right_waist = testDic[24]

    # right body vector
    right_body_vector = tupple_diff(right_waist, right_shoulder)


    left_shoulder = testDic[11]
    left_elbow = testDic[13]

    # left hand vector
    left_hand_vector = tupple_diff(left_elbow, left_shoulder)
    left_waist = testDic[23]

    # left body vector
    left_body_vector = tupple_diff(left_waist, left_shoulder)

    lAngle = angle_between(left_body_vector, left_hand_vector)
    rAngle = angle_between(right_body_vector, right_hand_vector)

    ################################################################
    # Computing angle formed by elbow

    # right upper hand vector
    right_upper_hand = tupple_diff(right_shoulder, right_elbow)
    right_wrist = testDic[16]

    # right lower hand bector
    right_lower_hand = tupple_diff(right_wrist, right_elbow)

    # left upper hand vector
    left_upper_hand = tupple_diff(left_shoulder, left_elbow)
    left_wrist = testDic[15]

    # left lower hand vector
    left_lower_hand = tupple_diff(left_wrist, left_elbow)

    lAngle2 = angle_between(left_lower_hand, left_upper_hand)
    rAngle2 = angle_between(right_lower_hand, right_upper_hand)


    return rAngle, lAngle, rAngle2, lAngle2


"""
This function computes the metric depending on the vector angles calculated.
It is based on the mean square method to compute the resultant metrics.
"""
def compute_metric(angleR, angleL, angleR2, angleL2):
    return (angleR**2 + angleL**2 + angleR2**2 + angleL2**2)**0.5


"""
This function takes the image, dictionary and the imageNames of the dataset as
input and returns the best fit image after comparing the input image with all
the images in the dataset.

First it detects the landmarks on the input image, then computes the angles which
are required for comparison for the landmarks detected.

Then it computes the angles for each image in the dataset using the list of
dictionary passed as input parameter. It checks the metrics and takes the metric
with least value and returns the best image selected by this method.
"""
def best_fit_image(image_, dataset_dict_list, imagesNames):
    # detecting landmarks for input image
    testDic, res = detect_landmarks(image_)
    rAngle, lAngle, rAngle1, lAngle1 = computeAngleLR(testDic)

    # initializing variables
    minnMetric = 10000
    bestFitImage = imagesNames[0]


    print(rAngle, rAngle)

    # looping over all the images in the dataset
    for i, dic in enumerate(dataset_dict_list):
        print("Co-relating with image ",imagesNames[i])
        rDataAngle, lDataAngle, rDataAngle1, lDataAngle1 =  computeAngleLR(dic)

        angleL = abs(rDataAngle - rAngle)
        angleR = abs(lDataAngle - lAngle)
        angleL1 = abs(rDataAngle1 - rAngle1)
        angleR1 = abs(lDataAngle1 - lAngle1)

        print( angleL, angleR, angleL1, angleR1)

        metTest = compute_metric(angleL, angleR,  angleL1, angleR1)

        # taking the image with least metric
        if (metTest < minnMetric):
            minnMetric = metTest
            bestFitImage = imagesNames[i]

    return bestFitImage, res


"""
This function computes the landmarks for each image in the dataset and stores
it in the list of dictionary having key vs tuples. It stores the 32 landmarks
detected by the pose estimation module of mediapipe for each image.

It takes the folder dataset path as input paramter and loops over all the images
in it. It returns the list of dictionary and images names in the directory.
"""
def dataset_landmarks(MAIN_FOLDER):

    dictionary_list = []
    imagesNames = []

    filenames = os.listdir(MAIN_FOLDER)

    # looping over all the images in dataset
    for filename in filenames:
        imgPath = os.path.join(MAIN_FOLDER, filename)

        # computing landmarks
        image_ = cv2.imread(imgPath)
        dict_, _ = detect_landmarks(image_)
        dictionary_list.append(dict_)
        imagesNames.append(filename)

        print("Dataset points computed for ",filename)

    return dictionary_list, imagesNames

"""
Function to plot the results in case the input given is an image and not video.
The demoImage shows the landmarks detected on the body by the mediapipe library.
It draws the pose on a black background using the draw_landmarks module of
drawing utilities in mediapipe.
"""
def plotResults(inpImage, bestFitImage, res):

    fig, axs = plt.subplots(1, 3)

    # input image
    axs[0].imshow(inpImage,cmap='gray')
    axs[0].set_title('Input Image')

    # landmarks detected
    demoImage = cv2.imread("black.jpg")
    demoImage = cv2.resize(demoImage,inpImage.shape[:2])
    mpDraw.draw_landmarks(demoImage,res.pose_landmarks,mpPose.POSE_CONNECTIONS)

    axs[1].imshow(demoImage)
    axs[1].set_title('Image Landmarks')

    # best fit image
    axs[2].imshow(bestFitImage,cmap='gray')
    axs[2].set_title('Best Fit Image')

    plt.show()


"""

This function is the main function to run the project, it takes input parameters
as the directory path for dataset, list of dictionaries, image paths, video_input
parameter which indicates whether the input is a video or an image. If input is
and image, it also takes the path of the input image.

Incase of a video input, for each frame, the landmarks are calculated and passed
to the function best fit to get the resultant character image for the current
frame and it is displayed to the user in the same format as done for the image.

Incase the input is an image, it detects the landmarks on it and similarly finds
the best fit image out of all the images in the dataset and plots the results.

"""
def projectRun(MAIN_FOLDER, lst, imageNames, video_input=1, image_input=None):

    # input given by user is the live video capturing by camera
    if (video_input == 1):
        cap = cv2.VideoCapture(0)
        while(True):
            _, img = cap.read()

            if img is None:
                continue

            bestFitImage, res = best_fit_image(img, lst, imageNames)
            bestFitImage = cv2.imread(os.path.join(MAIN_FOLDER,bestFitImage))

            cv2.imshow('out', img)

            demoImage = cv2.imread("black.jpg")
            # demoImage = np.ones(inpImage.shape)
            demoImage = cv2.resize(demoImage,img.shape[:2])
            mpDraw.draw_landmarks(demoImage,res.pose_landmarks,mpPose.POSE_CONNECTIONS)
            cv2.imshow('demo',demoImage)

            bestFitImage = cv2.resize(bestFitImage,(350,512))
            cv2.imshow('frame', bestFitImage)

            # press 'Q' if you want to exit
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    # input given by user is an image
    else:
        image_ = cv2.imread(image_input)
        bestFitImageName, res = best_fit_image(image_, lst, imageNames)
        print("Plotting best fit image results\n")
        bestFitImage = cv2.imread(os.path.join(MAIN_FOLDER,bestFitImageName))
        plotResults(image_, bestFitImage, res)

"""
MAIN FUNCTION :
First evaluates the input arguments, then computes landmarks for each image in
the dataset and runs the project using projectRun function.
"""
def main():

    # variables used in this function
    MAIN_FOLDER = 'data3'
    IMAGE_VIDEO = 1      # denotes video by default
    IMAGE_PATH = None

    # evaluating the arguments given by the user
    n = len(sys.argv)

    for arg in range(n):
        # if data path is changed than default
        if sys.argv[arg] == "--data":
            MAIN_FOLDER = sys.argv[arg+1]

        # if user wants to give input as an image instead of video, it
        # must specify the path of the input image.
        if sys.argv[arg] == "--image":
            IMAGE_VIDEO = 0      # changing default video input to image
            IMAGE_PATH = sys.argv[arg+1]

    print("\n------ Computing best fit image path for the input image ------\n")

    print("Computing dataset points...\n")
    lst, imageNames = dataset_landmarks(MAIN_FOLDER)

    # RUNNING THE PROJECT
    projectRun(MAIN_FOLDER,lst,imageNames,IMAGE_VIDEO,IMAGE_PATH)


if __name__ == "__main__":
    main()
