# Human Action Replication by Animated Character 

Owned By : Himani Jain [111801016] and Parul Sangwan [111801053]

Department : Computer Science and Engineering

Institute : IIT Palakkad

## Installations Required

```
pip3 install mediapipe
```

## Run the model


The python 3 application works on both video and image input.  The input is given below . Note that [] brackets represent the optional command line parameter.

Video Input
```
python3 pose.py [--data <Path to the dataset>]
```

Image input
```
python3 pose.py [--data <Path to the dataset>] --image <Path to image>
```

There are three windows that will be shown to the user on his screen as output -
1. The video being captured by the webcam on the user’s device.
2. The pose on a black background corresponding to the landmarks detected.
3. The best fit character image for the pose of the user captured in that frame.
